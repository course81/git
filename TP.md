# Prérequis

Pour ce TP il vous faut :
- Un compte sur Gitlab
- Un IDE comme VScode
- Installer Git sur votre machine

# Introduction

Pour commencer il faut créer un projet sur Gitlab.com , cliquez sur "New project" puis sélectionner "Blank project" et décochez la cache "Initialize repository with a README". Renseignez ensuite les autres champs comme vous le voulez. 


Maintenant que vous avez créer un projet sur Gitlab il est temps de le cloner sur votre machine avec la commande suivante :

:warning: **Le projet sera téléchargé dans votre répertoire courant, positionez vous au bon endroit.**
(Pour récupérer l'url du projet à cloner il faut cliquer sur "Clone" et sélectionner "Clone with HTTPS")

```bash
git clone <URL DU PROJET>
```

Maintenant que tout est ok, créez un nouveau fichier dans votre projet. 
Si vous utilisez un IDE, vous remarquerez que le nouveau fichier à une couleur différente. 

Pourquoi ça ? 

Utilisez la commande :

```bash
git status
```

Qu'est ce que cela nous indique ? Et comment faire en sorte de remédier à ça ? 

Maintenant il faut faire passer les modification de l'arbre de staging vers le répo local, pour ça il va falloir utiliser la commande : 

```bash
git commit -m "<MESSAGE DE COMMIT>" 
```

A quoi correspond l'option "-m" de la commande "commit" ? Est elle nécessaire ? 

Une fois cette opération réalisée, vous aller pousser les modifications de votre répo local vers le répo distant avec la commande : 

```bash
git push
```

Allez sur votre projet sur Gitlab, comment trouver le commit et quel est son numéro ? Où peut on voir les modifications de ce commit ? 

Effectuer une modification sur le fichier de votre projet puis pousser les modifications locales vers le répo distant.

Qu'avez vous du faire les 2 fois après l'exécution de la commande push ? 

Vous allez maintenant renseignez des variables dans votre git global comme votre nom d'utilisateur et votre email avec les commandes suivantes : 

```bash
git config --global user.name <USERNAME>
git config --global user.email <EMAIL>
```

Cela permettra de savoir qui a effectuer les modifications lors d'un commit. 

## Mettre en place une clé SSH 

Pour se faciliter la gestion et éviter de devoir taper son mot de passe à chaque push ou chaque clone/pull, nous allons mettre en place un clé SSH sur notre répo

Pour cela il va nous falloir un CMD et exécuter la commande :

```
ssh-keygen
```

Rendez vous ensuite dans les préférences de votre compte gitlab, dans la section SSH key, et renseigner le contenu du fichier ./ssh/id_rsa.pub dans le champs "key" puis ajouter votre clé. 

Vous pouvez maintenant utiliser votre répo sans avoir besoin de taper votre mot de passe grâce à la clé SSH. 


## Branche

Maintenant que les commandes de base et le setup de l'environnement Git on été vu nous allons passer à la gestion des branches

Comment s'appelle la branche de votre projet ? Quelle était la norme avant ? 

Vous allez maintenant créer une nouvelle branche avec la commande suivante : 

```bash
git checkout -b <NOM BRANCHE>
```

Pour vérifier que vous êtes sur la bonne branche effectuez la commande : 

```bash
git branch
```

Maintenant vous aller effectuer des modifications sur le fichier créé et les pousser sur le répo distant. 
Quelle erreur rencontrer vous lors du push ? Comment résoudre ça ? 

Rendez vous sur gitlab et effectuer une merge request, vous l'assigner et effectuer le merge de votre branche vers la branche master, en supprimant la branche lors du merge.

## Merge et Rebase

Maintenant il va falloir récupérer les modifications du merge en local, pour cela il faut aller sur la branche master et faire un pull. 

Maintenant que la branche est à jour, faites une branche de feature.
Retourner sur les merge request dans Gitlab, que voyez vous ? 

Pour remédier à ce conflit il va falloir effectuer un rebase, pour gérer les conflits en local, pour ensuite les repousser sur le répo distant.

Pour cela il faut utiliser la commande : 

```bash
git rebase --onto <BRANCHE SOURCE> HEAD~1
```
Le nombre après le HEAD correspondant au nombre de commit sur votre branche actuelle.

Puis il faut aller sur votre éditeur pour gérer les conflits, en acceptant ou non les changements.


### Rebase 

Une fois que vous avez fait cette opérations faites un grand nombre de commit (environ 10) et pushez le tout sur une nouvelle branche. Maintenant aller consulter le graphe de votre répo. Que voyez vous et comment pensez vous pouvoir améliorer ça ? 

Nous allons maintenant effectuer la commande suivante qui aura pour but de rebase la branche actuelle pour nous permettre de réécrire l'historique. 

```bash
git rebase -i HEAD~10
```

Nous effectuons ce que l'ont appelle un rebase intéractif (-i), nous allons alors avoir la liste des 10 derniers commit (HEAD~10) et nous pouvons effectuer des actions sur chacuns. Comme nous voulons réécrire l'historique de la branche pour supprimer des commit inutiles, nous allons "squash" les commit que nous ne voulons pas. Squash va avoir pour effet non pas de supprimer un commit, mais de le fusionner avec le commit précédent. 

L'objectif est donc de fusionner tous les commit que vous avez fait précédemment en un seul. Une fois le rebase terminé, il vous faut pousser les modifications sur le répo distant. Que se passe t-il lors du push ? Pourquoi et comment pouvez vous régler le problème ? 

Maintenant que nous savons comment faire le ménage sur une branche, nous allons apprendre comment ne faire n'importe quoi dans un premier temps. 

Vous aller recréer une branche, faire un nouveau commit et le push sur cette branche. Maintenant faites un nouveau commit mais au lieu d'utiliser la commande standard faites : 

```bash
git commit --amend
```

Vous aller avoir un éditeur de texte qui s'ouvre, vous pouvez choisir de modifier le message de commit si vous le souhaitez ou pas, sauvegardez les modifications et quittez. Vous venez d'éditer votre dernier commit pour prendre en compte les nouvelles modifications, mais cela a eu pour effet de changer le hash du commit. 

Maintenant pousser le travail sur votre répo, que se passe t-il ? Pourquoi ? Et comment résoudre ca SANS faire le bourrin ? 

## Les bonnes pratiques 

[Best pratice](https://www.conventionalcommits.org/en/v1.0.0/)

# Annexe 

Garder ses crédentials 

```bash
git config credential.helper store
```

Supprimer credential 

```bash
git config --global --unset credential.helper
```
